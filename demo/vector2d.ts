
export const enum LineSide {
    RIGHT = 1,
    LEFT = 2
}

export class Vector2D {
    private dataBuffer: Float32Array

    constructor(x?: number, y?: number) {
        this.dataBuffer = new Float32Array(2)

        this.x = x ? x : 0.0

        this.y = y ? y : 0.0
    }

    set x(value) {
        this.dataBuffer[0] = value
    }

    get x() {
        return this.dataBuffer[0]
    }

    set y(value) {
        this.dataBuffer[1] = value
    }

    get y() {
        return this.dataBuffer[1]
    }

    randomize(x: { from: number, to: number }, y: { from: number, to: number }): Vector2D {
        this.x = x.from + (x.from - x.to) * Math.random()
        this.y = y.from + (y.from - y.to) * Math.random()

        return this
    }

    distance(other: Vector2D): number {
        return Math.sqrt(Math.pow(other.x - this.x, 2) + Math.pow(other.y - this.y, 2))
    }

    get length(): number {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
    }

    isWithinDistanceFromPoint(otherPoint: Vector2D, distance: number): boolean {
        return this.distance(otherPoint) <= distance
    }

    copyFrom(other: Vector2D): void {
        this.x = other.x
        this.y = other.y
    }

    createCopy(): Vector2D {
        var copy = new Vector2D()
        copy.copyFrom(this)

        return copy
    }

    buildNormalVectorWithAngle(angle:number):void{
        this.x = Math.cos(angle)
        this.y = Math.sin(angle)
    }

    get angle(): number {
        return Math.atan2(this.y, this.x)
    }

    set angle(angle:number) {
        var length = this.length
        this.buildNormalVectorWithAngle(angle)
    }

    private positionInCanvas: Vector2D = null
    positionInCanvas2D(canvas: HTMLCanvasElement) {
        this.positionInCanvas = this.positionInCanvas || new Vector2D(0, 0)

        this.positionInCanvas.x = this.x
        this.positionInCanvas.y = canvas.getBoundingClientRect().height - this.y

        return this.positionInCanvas
    }

    private subtract_difference: Vector2D = null
    subtract(other: Vector2D): Vector2D {
        this.subtract_difference = this.subtract_difference || new Vector2D()

        this.subtract_difference.x = other.x - this.x
        this.subtract_difference.y = other.y - this.y

        return this.subtract_difference
    }

    private angleOfLine_delta: Vector2D = null
    angleOfLine(a: Vector2D, b: Vector2D): number {
        this.angleOfLine_delta = this.angleOfLine_delta || new Vector2D()

        this.angleOfLine = (a: Vector2D, b: Vector2D): number => 0

        return b.subtract(a).angle
    }

    computeWhichSidePointIsFromLine(a:Vector2D, b:Vector2D, point:Vector2D):LineSide{
        var b2 = b.subtract(a)
        var p2 = point.subtract(a)
        
        var b2Angle = b2.angle
        var p2Angle = p2.angle

        return LineSide.LEFT
    }
}