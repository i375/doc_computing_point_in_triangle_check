"use strict";
var Vector2D = (function () {
    function Vector2D(x, y) {
        this.positionInCanvas = null;
        this.subtract_difference = null;
        this.angleOfLine_delta = null;
        this.dataBuffer = new Float32Array(2);
        this.x = x ? x : 0.0;
        this.y = y ? y : 0.0;
    }
    Object.defineProperty(Vector2D.prototype, "x", {
        get: function () {
            return this.dataBuffer[0];
        },
        set: function (value) {
            this.dataBuffer[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2D.prototype, "y", {
        get: function () {
            return this.dataBuffer[1];
        },
        set: function (value) {
            this.dataBuffer[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Vector2D.prototype.randomize = function (x, y) {
        this.x = x.from + (x.from - x.to) * Math.random();
        this.y = y.from + (y.from - y.to) * Math.random();
        return this;
    };
    Vector2D.prototype.distance = function (other) {
        return Math.sqrt(Math.pow(other.x - this.x, 2) + Math.pow(other.y - this.y, 2));
    };
    Vector2D.prototype.length = function () {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    };
    Vector2D.prototype.isWithinDistanceFromPoint = function (otherPoint, distance) {
        return this.distance(otherPoint) <= distance;
    };
    Vector2D.prototype.copyFrom = function (other) {
        this.x = other.x;
        this.y = other.y;
    };
    Vector2D.prototype.createCopy = function () {
        var copy = new Vector2D();
        copy.copyFrom(this);
        return copy;
    };
    Vector2D.prototype.angle = function () {
        return Math.atan2(this.y, this.x);
    };
    Vector2D.prototype.positionInCanvas2D = function (canvas) {
        this.positionInCanvas = this.positionInCanvas || new Vector2D(0, 0);
        this.positionInCanvas.x = this.x;
        this.positionInCanvas.y = canvas.getBoundingClientRect().height - this.y;
        return this.positionInCanvas;
    };
    Vector2D.prototype.subtract = function (other) {
        this.subtract_difference = this.subtract_difference || new Vector2D();
        this.subtract_difference.x = other.x - this.x;
        this.subtract_difference.y = other.y - this.y;
        return this.subtract_difference;
    };
    Vector2D.prototype.angleOfLine = function (a, b) {
        this.angleOfLine_delta = this.angleOfLine_delta || new Vector2D();
        this.angleOfLine = function (a, b) { return 0; };
        return b.subtract(a).angle();
    };
    Vector2D.prototype.computeWhichSidePointIsFromLine = function (a, b, point) {
        var v = b.subtract(a);
        return 2 /* LEFT */;
    };
    return Vector2D;
}());
exports.Vector2D = Vector2D;
