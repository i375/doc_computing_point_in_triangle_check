//import * as gtree from "./gtree"

import { Vector2D, LineSide } from "./vector2d"

type Integer = number
type Decimal = number
type Index = Integer
type Point = Array<number>
type Vector = Point
type VectorLegth = Decimal
type ShortestVectorLength = VectorLegth
type ShortestVectorIndex = Index
type Line = Array<Point>
type ConvexHull = Array<Point>
type Triangle = ConvexHull

function pointInConvexHull(p: Point, h: ConvexHull): boolean {
    function vectorLength(v: Vector): VectorLegth {
        return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))
    }

    function shortestVectorIndex(v: Array<Vector>): ShortestVectorIndex {
        const vl: Array<VectorLegth> = v.map((v): VectorLegth => vectorLength(v))

        const shv: ShortestVectorLength = vl.reduce((p, c): VectorLegth => p < c ? p : c)

        const shvi: ShortestVectorIndex = vl.indexOf(shv)

        return shvi
    }

    function angleOfLine(l: Line): number {
        const a = l[0]
        const b = l[1]

        const dx = b[0] - a[0] // ∂x = b[x] - a[x]
        const dy = b[1] - a[1] // ∂y = b[y] - a[y]

        return Math.atan2(dy, dx)
    }

    return false
}

interface FrameEventsHandler {
    beforeFrame(): void
    afterFrame(): void
}

class InputManager implements FrameEventsHandler {
    canvas: HTMLCanvasElement
    mouseIsDown: boolean
    mouseWasDown: boolean

    mouseXY: Vector2D = new Vector2D(0, 0)

    constructor(canvas: HTMLCanvasElement) {
        this.mouseIsDown = false
        this.mouseWasDown = false

        canvas.addEventListener("mousedown", (e) => {
            this.mouseIsDown = true
        })

        canvas.addEventListener("mouseup", (e) => {
            this.mouseIsDown = false
        })

        canvas.addEventListener("mousemove", (e) => {
            this.mouseXY.x = e.clientX - canvas.offsetLeft
            this.mouseXY.y = canvas.getBoundingClientRect().height - (e.clientY - canvas.offsetTop)
        })
    }

    beforeFrame(): void {

    }
    afterFrame(): void {
        this.mouseWasDown = this.mouseIsDown
    }

}

interface Renderable {
    position: Vector2D
    render(ctx: CanvasRenderingContext2D): void
}

interface InteractiveElement extends Renderable {
    processInput(input: InputManager, frameState: FrameSate)
}

class InteractivePoint implements InteractiveElement {
    position: Vector2D
    pointWidth: number = 10
    COLOR_NORMAL = "#ff6666"
    COLOR_ACTIVE = "#66ff66"
    colorCurrent = this.COLOR_NORMAL

    constructor(x: number, y: number) {
        this.position = new Vector2D(x, y)
    }

    render(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = this.colorCurrent

        ctx.beginPath()
        ctx.arc(this.position.positionInCanvas2D(ctx.canvas).x, this.position.positionInCanvas2D(ctx.canvas).y, this.pointWidth, 0, 2 * Math.PI)
        ctx.fill()

        //ctx.fillRect(this.position.x - this.pointWidth / 2, this.position.y - this.pointWidth / 2, this.pointWidth, this.pointWidth)
    }

    processInput(input: InputManager, frameState: FrameSate) {
        let pointInRange = this.position.isWithinDistanceFromPoint(input.mouseXY, 10)

        if (pointInRange) {
            this.colorCurrent = this.COLOR_ACTIVE
        }
        else {
            this.colorCurrent = this.COLOR_NORMAL
        }

        if (frameState.selectedElem === null && pointInRange && input.mouseIsDown) {
            frameState.selectedElem = this
        }

        if (frameState.selectedElem === this) {
            this.position.copyFrom(input.mouseXY)
        }

        if (frameState.selectedElem === this && input.mouseIsDown == false) {
            frameState.selectedElem = null
        }
    }
}

class FrameSate {
    selectedElem: InteractiveElement = null
}

function drawTriangle(ctx: CanvasRenderingContext2D, trianglePoints: Array<InteractivePoint>, userPoint: InteractivePoint) {

    const COLOR_POINT_OUTSIDE = "#ff6666"
    const COLOR_POINT_INSIDE = "#66ff66"

    ctx.save()

    ctx.beginPath()
    ctx.fillStyle = COLOR_POINT_OUTSIDE

    let counter = 0

    var point = trianglePoints[counter++].position
    ctx.moveTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y)

    point = trianglePoints[counter++].position
    ctx.lineTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y)

    point = trianglePoints[counter++].position
    ctx.lineTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y)
    
    ctx.fill()

    ctx.restore()
} 



window.addEventListener("load", (ev: Event) => {

    var canvas = <HTMLCanvasElement>document.getElementById("main-canvas")
    var ctx2d = canvas.getContext("2d")
    var canvasSize = {
        w: canvas.width,
        h: canvas.height
    }

    var inputManager = new InputManager(canvas)
    var state = new FrameSate()

    var trianglePoints: Array<InteractivePoint> = [new InteractivePoint(100, 100), new InteractivePoint(200, 100), new InteractivePoint(150, 200)]
    var userPoint = new InteractivePoint(canvasSize.w / 2, canvasSize.h / 2);

    var interactivePoints = <Array<InteractivePoint>>[]
    interactivePoints.push(trianglePoints[0])
    interactivePoints.push(trianglePoints[1])
    interactivePoints.push(trianglePoints[2])
    interactivePoints.push(userPoint)

    var nearestHoveredPoint: Vector2D = null

    type PointerInCanvasCoordinate = Vector2D

    let debugText = <HTMLTextAreaElement>document.getElementById("debug-text")

    let frameEventHandlers = <Array<FrameEventsHandler>>[]
    frameEventHandlers.push(inputManager)

        ; (function mainLoop() {

            frameEventHandlers.forEach((a) => a.beforeFrame())

            ctx2d.clearRect(0, 0, canvasSize.w, canvasSize.h)

            drawTriangle(ctx2d, trianglePoints, userPoint)

            interactivePoints.forEach((p) => {
                p.processInput(inputManager, state)
                p.render(ctx2d)
            })

            debugText.innerText = `<pre>${JSON.stringify(inputManager.mouseXY)}</pre>`

            frameEventHandlers.forEach((a) => a.afterFrame())

            requestAnimationFrame(mainLoop)
        })()

})