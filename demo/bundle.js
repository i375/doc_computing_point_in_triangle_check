(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
//import * as gtree from "./gtree"
"use strict";
var vector2d_1 = require("./vector2d");
function pointInConvexHull(p, h) {
    function vectorLength(v) {
        return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2));
    }
    function shortestVectorIndex(v) {
        var vl = v.map(function (v) { return vectorLength(v); });
        var shv = vl.reduce(function (p, c) { return p < c ? p : c; });
        var shvi = vl.indexOf(shv);
        return shvi;
    }
    function angleOfLine(l) {
        var a = l[0];
        var b = l[1];
        var dx = b[0] - a[0]; // ∂x = b[x] - a[x]
        var dy = b[1] - a[1]; // ∂y = b[y] - a[y]
        return Math.atan2(dy, dx);
    }
    return false;
}
var InputManager = (function () {
    function InputManager(canvas) {
        var _this = this;
        this.mouseXY = new vector2d_1.Vector2D(0, 0);
        this.mouseIsDown = false;
        this.mouseWasDown = false;
        canvas.addEventListener("mousedown", function (e) {
            _this.mouseIsDown = true;
        });
        canvas.addEventListener("mouseup", function (e) {
            _this.mouseIsDown = false;
        });
        canvas.addEventListener("mousemove", function (e) {
            _this.mouseXY.x = e.clientX - canvas.offsetLeft;
            _this.mouseXY.y = canvas.getBoundingClientRect().height - (e.clientY - canvas.offsetTop);
        });
    }
    InputManager.prototype.beforeFrame = function () {
    };
    InputManager.prototype.afterFrame = function () {
        this.mouseWasDown = this.mouseIsDown;
    };
    return InputManager;
}());
var InteractivePoint = (function () {
    function InteractivePoint(x, y) {
        this.pointWidth = 10;
        this.COLOR_NORMAL = "#ff6666";
        this.COLOR_ACTIVE = "#66ff66";
        this.colorCurrent = this.COLOR_NORMAL;
        this.position = new vector2d_1.Vector2D(x, y);
    }
    InteractivePoint.prototype.render = function (ctx) {
        ctx.fillStyle = this.colorCurrent;
        ctx.beginPath();
        ctx.arc(this.position.positionInCanvas2D(ctx.canvas).x, this.position.positionInCanvas2D(ctx.canvas).y, this.pointWidth, 0, 2 * Math.PI);
        ctx.fill();
        //ctx.fillRect(this.position.x - this.pointWidth / 2, this.position.y - this.pointWidth / 2, this.pointWidth, this.pointWidth)
    };
    InteractivePoint.prototype.processInput = function (input, frameState) {
        var pointInRange = this.position.isWithinDistanceFromPoint(input.mouseXY, 10);
        if (pointInRange) {
            this.colorCurrent = this.COLOR_ACTIVE;
        }
        else {
            this.colorCurrent = this.COLOR_NORMAL;
        }
        if (frameState.selectedElem === null && pointInRange && input.mouseIsDown) {
            frameState.selectedElem = this;
        }
        if (frameState.selectedElem === this) {
            this.position.copyFrom(input.mouseXY);
        }
        if (frameState.selectedElem === this && input.mouseIsDown == false) {
            frameState.selectedElem = null;
        }
    };
    return InteractivePoint;
}());
var FrameSate = (function () {
    function FrameSate() {
        this.selectedElem = null;
    }
    return FrameSate;
}());
function drawTriangle(ctx, trianglePoints, userPoint) {
    var COLOR_POINT_OUTSIDE = "#ff6666";
    var COLOR_POINT_INSIDE = "#66ff66";
    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = COLOR_POINT_OUTSIDE;
    var counter = 0;
    var point = trianglePoints[counter++].position;
    ctx.moveTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y);
    point = trianglePoints[counter++].position;
    ctx.lineTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y);
    point = trianglePoints[counter++].position;
    ctx.lineTo(point.positionInCanvas2D(ctx.canvas).x, point.positionInCanvas2D(ctx.canvas).y);
    ctx.fill();
    ctx.restore();
}
window.addEventListener("load", function (ev) {
    var canvas = document.getElementById("main-canvas");
    var ctx2d = canvas.getContext("2d");
    var canvasSize = {
        w: canvas.width,
        h: canvas.height
    };
    var inputManager = new InputManager(canvas);
    var state = new FrameSate();
    var trianglePoints = [new InteractivePoint(100, 100), new InteractivePoint(200, 100), new InteractivePoint(150, 200)];
    var userPoint = new InteractivePoint(canvasSize.w / 2, canvasSize.h / 2);
    var interactivePoints = [];
    interactivePoints.push(trianglePoints[0]);
    interactivePoints.push(trianglePoints[1]);
    interactivePoints.push(trianglePoints[2]);
    interactivePoints.push(userPoint);
    var nearestHoveredPoint = null;
    var debugText = document.getElementById("debug-text");
    var frameEventHandlers = [];
    frameEventHandlers.push(inputManager);
    (function mainLoop() {
        frameEventHandlers.forEach(function (a) { return a.beforeFrame(); });
        ctx2d.clearRect(0, 0, canvasSize.w, canvasSize.h);
        drawTriangle(ctx2d, trianglePoints, userPoint);
        interactivePoints.forEach(function (p) {
            p.processInput(inputManager, state);
            p.render(ctx2d);
        });
        debugText.innerText = "<pre>" + JSON.stringify(inputManager.mouseXY) + "</pre>";
        frameEventHandlers.forEach(function (a) { return a.afterFrame(); });
        requestAnimationFrame(mainLoop);
    })();
});

},{"./vector2d":2}],2:[function(require,module,exports){
"use strict";
var Vector2D = (function () {
    function Vector2D(x, y) {
        this.positionInCanvas = null;
        this.subtract_difference = null;
        this.angleOfLine_delta = null;
        this.dataBuffer = new Float32Array(2);
        this.x = x ? x : 0.0;
        this.y = y ? y : 0.0;
    }
    Object.defineProperty(Vector2D.prototype, "x", {
        get: function () {
            return this.dataBuffer[0];
        },
        set: function (value) {
            this.dataBuffer[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2D.prototype, "y", {
        get: function () {
            return this.dataBuffer[1];
        },
        set: function (value) {
            this.dataBuffer[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Vector2D.prototype.randomize = function (x, y) {
        this.x = x.from + (x.from - x.to) * Math.random();
        this.y = y.from + (y.from - y.to) * Math.random();
        return this;
    };
    Vector2D.prototype.distance = function (other) {
        return Math.sqrt(Math.pow(other.x - this.x, 2) + Math.pow(other.y - this.y, 2));
    };
    Vector2D.prototype.length = function () {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    };
    Vector2D.prototype.isWithinDistanceFromPoint = function (otherPoint, distance) {
        return this.distance(otherPoint) <= distance;
    };
    Vector2D.prototype.copyFrom = function (other) {
        this.x = other.x;
        this.y = other.y;
    };
    Vector2D.prototype.createCopy = function () {
        var copy = new Vector2D();
        copy.copyFrom(this);
        return copy;
    };
    Vector2D.prototype.angle = function () {
        return Math.atan2(this.y, this.x);
    };
    Vector2D.prototype.positionInCanvas2D = function (canvas) {
        this.positionInCanvas = this.positionInCanvas || new Vector2D(0, 0);
        this.positionInCanvas.x = this.x;
        this.positionInCanvas.y = canvas.getBoundingClientRect().height - this.y;
        return this.positionInCanvas;
    };
    Vector2D.prototype.subtract = function (other) {
        this.subtract_difference = this.subtract_difference || new Vector2D();
        this.subtract_difference.x = other.x - this.x;
        this.subtract_difference.y = other.y - this.y;
        return this.subtract_difference;
    };
    Vector2D.prototype.angleOfLine = function (a, b) {
        this.angleOfLine_delta = this.angleOfLine_delta || new Vector2D();
        this.angleOfLine = function (a, b) { return 0; };
        return b.subtract(a).angle();
    };
    Vector2D.prototype.computeWhichSidePointIsFromLine = function (a, b, point) {
        var v = b.subtract(a);
        return 2 /* LEFT */;
    };
    return Vector2D;
}());
exports.Vector2D = Vector2D;

},{}]},{},[1]);
