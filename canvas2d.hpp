#if !defined(canvas2d_hpp)
#define canvas2d_hpp

#include <typeinfo>
#include <emscripten.h>
#include "cppjsbridge.hpp"

typedef int ResourceId;
typedef ResourceId context2d;

enum Canvas2DLineJoin
{
    BEVEL = 0,
    ROUND = 1,
    MITER = 2
};

const char *Canvas2DLineJoinStrings[] = {"bevel", "round", "miter"};

class Canvas2D
{
  private:
    ResourceId canvas;
    context2d ctx2d;

  public:
    Canvas2D(int width, int height)
    {
        initJS();

        this->canvas = EM_ASM_INT({
            var ResourceId = window._0[0];
            var canvas = document.createElement("canvas");
            canvas.width = $0;
            canvas.height = $1;
            document.body.appendChild(canvas);
            window._0[ResourceId] = canvas;

            window._0[0] = window._0[0] + 1;

            return ResourceId;
        },
                                  width, height);

        this->ctx2d = EM_ASM_INT({
            var ctxResourceId = window._0[0];
            var canvas = window._0[$0];
            var ctx2d = canvas.getContext('2d');
            window._0[ctxResourceId] = ctx2d;

            window._0[0] = window._0[0] + 1;

            return ctxResourceId;
        },
                                 this->canvas);
    }

    ~Canvas2D (){
        printf("Destructing %s\n", typeid(this).name());
    }

    ResourceId getResourceId()
    {
        return this->canvas;
    }

    void setFillStyle(const char *style)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.fillStyle = Module.UTF8ToString($1);
        },
                this->ctx2d, style);
    }

    void setStrokeStyle(const char *style)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.strokeStyle = Module.UTF8ToString($1);
        },
                this->ctx2d, style);
    }

    void setLineWidth(int width)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.lineWidth = $1;
        },
                this->ctx2d, width);
    }

    void fillRect(int x, int y, int width, int height)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.fillRect($1, $2, $3, $4);
        },
                this->ctx2d, x, y, width, height);
    }

    void clearRect(int x, int y, int width, int height)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.clearRect($1, $2, $3, $4);
        },
                this->ctx2d, x, y, width, height);
    }

    void beginPath()
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.beginPath();
        },
                this->ctx2d);
    }

    void setLineJoin(Canvas2DLineJoin lineJoin)
    {
        const char *lineJointString = Canvas2DLineJoinStrings[lineJoin];

        printf("%s", lineJointString);

        EM_ASM_({
            var ctx = window._0[$0];
            ctx.lineJoin = Module.UTF8ToString($1);
        },
                this->ctx2d, lineJointString);
    }

    void moveTo(int x, int y)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.moveTo($1, $2);
        },
                this->ctx2d, x, y);
    }

    void lineTo(int x, int y)
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.lineTo($1, $2);
        },
                this->ctx2d, x, y);
    }

    void closePath()
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.closePath();
        },
                this->ctx2d);
    }

    void stroke()
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.stroke();
        },
                this->ctx2d);
    }

    void fill()
    {
        EM_ASM_({
            var ctx = window._0[$0];
            ctx.fill();
        },
                this->ctx2d);
    }
};

#endif