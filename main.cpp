#include <emscripten.h>
#include <cstdio>
#include <memory>
#include "canvas2d.hpp"
#include "vector2d.hpp"
#include "canvas2devents.hpp"

class SharedState{
    private:

    public:
    std::shared_ptr<Canvas2D> c2d;
    std::shared_ptr<Canvas2DEvents> eventsManager;
};

extern "C" int main()
{
    auto c2d = std::make_shared<Canvas2D>(800, 600);
    c2d->setFillStyle("red");
    c2d->setStrokeStyle("green");
    c2d->fillRect(10, 10, 50, 50);

    c2d->setLineJoin(BEVEL);
    c2d->setLineWidth(10);

    c2d->beginPath();
    c2d->moveTo(200, 200);
    c2d->lineTo(300, 200);
    c2d->lineTo(200, 300);
    c2d->stroke();

    auto c2dEv = std::make_shared<Canvas2DEvents>(*c2d);

    auto sharedState = std::make_shared<SharedState>();
    sharedState->c2d = c2d;
    sharedState->eventsManager = c2dEv;

    emscripten_set_main_loop_arg([](void* state) {
        static std::shared_ptr<SharedState> sharedState = *static_cast<std::shared_ptr<SharedState>*>(state);
        static int frameCount = 0;

        printf("Moue x: %d, y: %d\n", sharedState->eventsManager->getMouseX(), sharedState->eventsManager->getMouseY());
        
    },
    &sharedState ,0, 1);

    printf("hello\n");
    return 0;
}