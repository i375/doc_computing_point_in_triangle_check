emcc -s WASM=1 \
--std=c++11 \
-s EXPORTED_FUNCTIONS="['_main', '_Canvas2DEvents_OnMouseMove', '_Canvas2DEvents_OnMouseUp', '_Canvas2DEvents_OnMouseDown','_Canvas2DEvents_OnKeyDown','_Canvas2DEvents_OnKeyUp']" \
-s EXTRA_EXPORTED_RUNTIME_METHODS="['UTF8ToString', 'stringToUTF8', 'setValue', 'getValue', 'ccall', 'cwrap']" \
main.cpp -o main.js