#include <emscripten.h>

void initJS()
{
    static int initialized = 0;

    if (initialized == 0)
    {
        EM_ASM({
            window._0 = [];
            window._0.length = 1024;
            window._0[0] = 1; // This is ResourceIndex
        });

        initialized = 1;
    }
}

