#if !defined(vector2d_hpp)
#define vector2d_hpp

#include <cmath>
#include <cstdlib>

enum LineSide
{
    RIGHT = 1,
    LEFT = 2
};

class Vector2D
{
  private:
    float dataBuffer[2];

  public:
    Vector2D() : Vector2D(0, 0)
    {
    }

    Vector2D(float x, float y)
    {
        this->setX(x);
        this->setY(y);
    }

    inline void setX(float x)
    {
        this->dataBuffer[0] = x;
    }

    inline float getX()
    {
        return this->dataBuffer[0];
    }

    inline void setY(float y)
    {
        this->dataBuffer[1] = y;
    }

    inline float getY()
    {
        return this->dataBuffer[1];
    }

    Vector2D &randomize(float xFrom, float xTo, float yFrom, float yTo)
    {
        this->setX(xFrom + (xFrom - xTo) * ((float)rand() / (float)RAND_MAX));
        this->setY(yFrom + (yFrom - yTo) * (rand() / (float)RAND_MAX));

        return *this;
    }

    float distance(Vector2D &other)
    {
        return sqrt(pow(other.getX() - this->getX(), 2) + pow(other.getY() - this->getY(), 2));
    }

    float getLength()
    {
        return sqrt(pow(this->getX(), 2) + pow(this->getY(), 2));
    }

    bool isWithinDistanceFromPoint(Vector2D &otherPoint, float distance)
    {
        return (this->distance(otherPoint) <= distance);
    }

    void copyFrom(Vector2D &other)
    {
        this->setX(other.getX());
        this->setY(other.getY());
    }

    Vector2D *createCopy()
    {
        Vector2D *copy = new Vector2D();
        copy->copyFrom(*this);

        return copy;
    }

    void buildNormalVectorWithAngle(float angle)
    {
        this->setX(cos(angle));
        this->setY(sin(angle));
    }

    float getAngle()
    {
        return atan2(this->getY(), this->getX());
    }

    void setAngle(float angle)
    {
        float length = this->getLength();
        this->buildNormalVectorWithAngle(angle);
        this->setX(this->getX() * length);
        this->setY(this->getY() * length);
    }

    // positionInCanvas2D(canvas: HTMLCanvasElement) {
    //     this->positionInCanvas = this->positionInCanvas || new Vector2D(0, 0)

    //     this->positionInCanvas.x = this->x;
    //     this->positionInCanvas.y = canvas.getBoundingClientRect().height - this->y

    //     return this->positionInCanvas
    // }
    Vector2D subtract(Vector2D &other)
    {
        Vector2D subtractionResult;

        subtractionResult.setX(other.getX() - this->getX());
        subtractionResult.setY(other.getY() - this->getY());

        return subtractionResult;
    }

    float angleOfLine(Vector2D &a, Vector2D &b)
    {
        return b.subtract(a).getAngle();
    }

    LineSide computeWhichSidePointIsFromLine(Vector2D &a, Vector2D &b, Vector2D &point)
    {
        auto b2 = b.subtract(a);
        auto p2 = point.subtract(a);

        auto b2Angle = b2.getAngle();
        auto p2Angle = p2.getAngle();

        return LEFT;
    }
};

#endif