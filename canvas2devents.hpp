#if !defined(canvas2devents_hpp)
#define canvas2devents_hpp

#include "canvas2d.hpp"
#include <cstdio>
#include <typeinfo>

class Canvas2DEvents;

// https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
typedef char HTMLKey;

extern "C" {
void Canvas2DEvents_OnMouseDown(Canvas2DEvents *_this);
void Canvas2DEvents_OnMouseUp(Canvas2DEvents *_this);
void Canvas2DEvents_OnMouseMove(Canvas2DEvents *_this, int x, int y);
void Canvas2DEvents_OnKeyDown(Canvas2DEvents *_this, HTMLKey* key);
void Canvas2DEvents_OnKeyUp(Canvas2DEvents *_this, HTMLKey* key);
}

class Canvas2DEvents
{
  public:
    Canvas2DEvents(Canvas2D &canvas) : canvas(canvas),
                                       mouseIsDown(false),
                                       mouseX(0),
                                       mouseY(0)
    {
        initJS();

        EM_ASM_(/*Hook up JS event handlers and redirection to C code*/ {

            var canvas = window._0[$1];
            var keyStringOutPtr = $2;
            var Canvas2DEvents_OnMouseDown = Module.cwrap("Canvas2DEvents_OnMouseDown", null, ['number']);
            var Canvas2DEvents_OnMouseUp = Module.cwrap("Canvas2DEvents_OnMouseDown", null, ['number']);
            var Canvas2DEvents_OnMouseMove = Module.cwrap("Canvas2DEvents_OnMouseMove", null, [ 'number', 'number', 'number' ]);
            var Canvas2DEvents_OnKeyDown = Module.cwrap("Canvas2DEvents_OnKeyDown", null,['number','number']);
            var Canvas2DEvents_OnKeyUp = Module.cwrap("Canvas2DEvents_OnKeyUp", null,['number','number']);

            var ownerPointer = $0;

            canvas.addEventListener("mousedown", function(e) {
                Canvas2DEvents_OnMouseDown(ownerPointer);
                e.preventDefault();
            });

            canvas.addEventListener("mouseup", function(e) {
                Canvas2DEvents_OnMouseUp(ownerPointer);
                e.preventDefault();
            });

            canvas.addEventListener("mousemove", function(e) {
                var mouseX = e.clientX - canvas.offsetLeft;
                var mouseY = canvas.getBoundingClientRect().height - (e.clientY - canvas.offsetTop);

                Canvas2DEvents_OnMouseMove(ownerPointer, mouseX, mouseY);
                e.preventDefault();
            });

            document.addEventListener("focus", function(e){
                console.log("canvas focused");
            });

            document.addEventListener("blur", function(e){
                console.log("canvas blurred");
            });

            document.addEventListener("keydown", function(e) {
                //console.log(`keydown: ${e.key}`);
                Module.stringToUTF8(e.key,keyStringOutPtr,24);
                Canvas2DEvents_OnKeyDown(ownerPointer, keyStringOutPtr);
                e.preventDefault();
            });

            document.addEventListener("keyup", function(e){
                //console.log(`keyup: ${e.key}`);
                Module.stringToUTF8(e.key,keyStringOutPtr,24);
                Canvas2DEvents_OnKeyUp(ownerPointer, keyStringOutPtr);
                e.preventDefault();
            });

        },
                this, this->canvas.getResourceId(), this->key);
    }

    ~Canvas2DEvents(){
        printf("Destructing %s\n", typeid(this).name());
    }

  private:
    ResourceId resourceId;
    Canvas2D &canvas;
    bool mouseIsDown;
    int mouseX;
    int mouseY;

    HTMLKey key[32];

    friend void Canvas2DEvents_OnMouseDown(Canvas2DEvents *_this);
    friend void Canvas2DEvents_OnMouseUp(Canvas2DEvents *_this);
    friend void Canvas2DEvents_OnMouseMove(Canvas2DEvents *_this, int x, int y);
    friend void Canvas2DEvents_OnKeyDown(Canvas2DEvents *_this, HTMLKey* key);
    friend void Canvas2DEvents_OnKeyUp(Canvas2DEvents *_this, HTMLKey* key);   

    public:
    inline decltype(mouseX) getMouseX(){
        return mouseX;
    }     

    inline decltype(mouseY) getMouseY(){
        return mouseY;
    }
};

extern "C" {
void Canvas2DEvents_OnMouseDown(Canvas2DEvents *_this)
{
    _this->mouseIsDown = true;
}

void Canvas2DEvents_OnMouseUp(Canvas2DEvents *_this)
{
    _this->mouseIsDown = false;
}

void Canvas2DEvents_OnMouseMove(Canvas2DEvents *_this, int x, int y)
{
    _this->mouseX = x;
    _this->mouseY = y;
}

void Canvas2DEvents_OnKeyDown(Canvas2DEvents *_this, HTMLKey* key)
{
    printf("onKeyDown: %s\n", key);
}

void Canvas2DEvents_OnKeyUp(Canvas2DEvents *_this, HTMLKey* key)
{
    printf("onKeyUp: %s\n", key);
}

}
#endif